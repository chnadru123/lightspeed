# -*- coding: utf-8 -*-
# Copyright (c) 2020, Raaj Tailor and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document
import requests
from frappe import _

class LightSpeedCredentials(Document):
	def import_items(self):
		api_key = frappe.db.get_value("Light Speed Credentials",None,"api_key")
		api_secret = frappe.db.get_value("Light Speed Credentials",None,"api_secret")
		if not api_key or not api_secret:
			raise frappe.ValidationError(_("Light Speed Is Not Confugured"))		
		
		try:
			response = requests.get('https://api.webshopapp.com/nl/products.json', auth=(api_key, api_secret))
			
			
			json_response = response.json()['products']
			# frappe.msgprint(str(response.json()['products']))
			for product in json_response:
				if not frappe.db.exists("Item", product['id']):
					self.create_item({
						"item_code":product['id'],
						"item_name":product['fulltitle'],
						"description":product['description']
					})
					frappe.msgprint(str(product['title'])+ "Created!")
					break
				
		except requests.exceptions.HTTPError:
			frappe.throw(_("Could not create Item On Light Speed!"))

	def create_item(self,data):
		doc = frappe.get_doc({
		"doctype": "Item",
		"item_code": str(data['item_code']),
		"item_name": str(data['item_name']),
		"description":str(data['description']),
		"item_group":"Products",
		"created_from_website":1,
		"stock_uom":"Nos"
		})
		doc.insert()
