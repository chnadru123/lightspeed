// Copyright (c) 2020, Raaj Tailor and contributors
// For license information, please see license.txt

frappe.ui.form.on('Light Speed Credentials', {
	// refresh: function(frm) {

	// }
	item_post:function(frm){
		frappe.call({
			method:"lightspeed.lightspeed.item.after_insert",
			callback:function(res){
				console.log(res)
			}
		})
	}
});
